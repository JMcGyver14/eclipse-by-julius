/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.BooleanControl;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
/**
 *
 * @author Jose Carlo Bulaclac
 */


public class Sound {
    
    public Clip clip;
    private String fileString;

    public Sound(String musicFile) {
        try {
            this.fileString = musicFile;
            InputStream is = getClass().getResourceAsStream(fileString);
            InputStream bis = new BufferedInputStream(is);
            AudioInputStream audio = AudioSystem.getAudioInputStream(bis);
            clip = AudioSystem.getClip();
            clip.open(audio);
           
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            System.out.println(e);
        }
    }

    public void playSound() {
        
        clip.start();
    }
    
    public void stopSound() {
        
        clip.stop();
    }
    
    
}
    
    

