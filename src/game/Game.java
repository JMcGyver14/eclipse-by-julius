/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
/**
 *
 * @author JM
 */
public class Game {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        JFrame window = new JFrame("Eclipse");
        
        
        JMenuBar mb = new JMenuBar();
        
        JMenu file = new JMenu("File");
        mb.add(file);
        JMenu edit = new JMenu("Edit");
        mb.add(edit);
        JMenuItem exit = new JMenuItem("Exit");
        file.add(exit);
        
        window.setJMenuBar(mb);
        
        new StartMenu().setVisible(true);
    }
}
